import {Component} from '@angular/core';

@Component({
  selector: 'ngclass-sample',
  templateUrl: './ngclass-sample.component.html',
  styleUrls: ['./ngclass-sample.component.css']
})
export class NgclassSampleComponent {

  public isBordered: boolean = true;
  public classesObj: Object;
  public classList: string[] = ['blue', 'round'];

  constructor() {
    this.toggleBorder();
  }

  public toggleBorder(): void {
    this.isBordered = !this.isBordered;
    this.classesObj = {
      bordered: this.isBordered
    };
  }

  public toggleClass(cssClassName: string): void {
    const pos: number = this.classList.indexOf(cssClassName);
    if (pos > -1) {
      this.classList.splice(pos, 1);
    } else {
      this.classList.push(cssClassName);
    }
  }

}

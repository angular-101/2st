import {Component} from '@angular/core';

@Component({
  selector: 'ngstyle-sample',
  templateUrl: './ngstyle-sample.component.html',
  styleUrls: ['./ngstyle-sample.component.css']
})
export class NgstyleSampleComponent {

  public color: string;

  public fontSize: number = 16;

  public style: {
    'background-color': string,
    'border-radius': string,
    border?: string,
    width?: string,
    height?: string
  } = {
    'background-color': '#ccc',
    'border-radius': '50px',
    'height': '30px',
    'width': '30px'
  };

  public apply(color: string, fontSize: number): void {
    this.color = color;
    this.fontSize = fontSize;
  }

}

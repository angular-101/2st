import {Component} from '@angular/core';

@Component({
  selector: 'ngnonbindable-sample',
  templateUrl: './ngnonbindable-sample.component.html',
  styleUrls: ['./ngnonbindable-sample.component.css']
})
export class NgnonbindableSampleComponent {

  public content: string = 'Text ...';

}

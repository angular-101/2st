import {Component} from '@angular/core';

@Component({
  selector: 'ngfor-sample',
  templateUrl: './ngfor-sample.component.html',
  styleUrls: ['./ngfor-sample.component.css']
})
export class NgforSampleComponent {

  public cities: string[] = ['Miami', 'Sao Paulo', 'New York'];

  public people: Object[] = [
    {name: 'Anderson', age: 35, city: 'Sao Paulo'},
    {name: 'John', age: 12, city: 'Miami'},
    {name: 'Peter', age: 22, city: 'New York'}
  ];

  public peopleByCity: Object = [
    {
      city: 'Miami',
      people: [
        {name: 'John', age: 12},
        {name: 'Angel', age: 22}
      ]
    },
    {
      city: 'Sao Paulo',
      people: [
        {name: 'Anderson', age: 35},
        {name: 'Felipe', age: 36}
      ]
    }
  ];

}

import {Component} from '@angular/core';

@Component({
  selector: 'ngif-sample',
  templateUrl: './ngif-sample.component.html',
  styleUrls: ['./ngif-sample.component.css']
})
export class NgifSampleComponent {

  public num1: number = 1;
  public num2: number = 2;
  public str: string = 'test';

  private _isTrue: boolean = true;

  public get isTrue(): boolean {
    return this._isTrue;
  }
}

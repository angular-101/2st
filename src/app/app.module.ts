import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppComponent} from './app.component';
import {FormsModule} from "@angular/forms";
import {NgclassSampleComponent} from "./ngclass-sample/ngclass-sample.component";
import {NgforSampleComponent} from "./ngfor-sample/ngfor-sample.component";
import {NgifSampleComponent} from "./ngif-sample/ngif-sample.component";
import {NgnonbindableSampleComponent} from "./ngnonbindable-sample/ngnonbindable-sample.component";
import {NgstyleSampleComponent} from "./ngstyle-sample/ngstyle-sample.component";
import {NgswitchSampleComponent} from "./ngswitch-sample/ngswitch-sample.component";
import {RouterModule} from "@angular/router";

@NgModule({
  declarations: [
    AppComponent,
    NgclassSampleComponent,
    NgforSampleComponent,
    NgifSampleComponent,
    NgnonbindableSampleComponent,
    NgstyleSampleComponent,
    NgswitchSampleComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    RouterModule.forRoot([
      {
        path: '', redirectTo: 'if', pathMatch: 'full'
      },
      {
        path: 'if', component: NgifSampleComponent
      },
      {
        path: 'for', component: NgforSampleComponent
      },
      {
        path: 'class', component: NgclassSampleComponent
      },
      {
        path: 'style', component: NgstyleSampleComponent
      },
      {
        path: 'switch', component: NgswitchSampleComponent
      },
      {
        path: 'non', component: NgnonbindableSampleComponent
      }
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}

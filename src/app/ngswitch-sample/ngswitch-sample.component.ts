import {Component} from '@angular/core';

@Component({
  selector: 'ngswitch-sample',
  templateUrl: './ngswitch-sample.component.html',
  styleUrls: ['./ngswitch-sample.component.css']
})
export class NgswitchSampleComponent {

  public choice: number = 1;

  public nextChoice(): void {
    this.choice += 1;

    if (this.choice > 5) {
      this.choice = 1;
    }
  }

}
